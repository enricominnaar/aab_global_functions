import 'package:aab_localized_copy/aab_localized_copy.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

import 'constants.dart';

/// Methods that are used across the entire application.

/// Converts the number of the month [i] to a String representing it.
String intToMonth(int i, context) {
  switch (i) {
    case 1:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'january', context);
    case 2:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'february', context);
    case 3:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'march', context);
    case 4:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'april', context);
    case 5:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'may', context);
    case 6:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'june', context);
    case 7:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'july', context);
    case 8:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'august', context);
    case 9:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'september', context);
    case 10:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'october', context);
    case 11:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'november', context);
    case 12:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'december', context);
  }
  return "";
}

/// Converts the number of the week [i] to a String representing it.
String intToWeek(int i, context) {
  switch (i) {
    case 1:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'monday', context);
    case 2:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'tuesday', context);
    case 3:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'wednesday', context);
    case 4:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'thursday', context);
    case 5:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'friday', context);
    case 6:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'saturday', context);
    case 7:
      return getLocalizedCopy(packageIdentifierAabGlobalFunctions, 'sunday', context);
  }
  return "";
}

/// Converts the day name to integer (e.g. Thursday -> 2)
int weekToInt(String day) {
  switch (day) {
    case "Monday":
      return 1;
    case "Tuesday":
      return 2;
    case "Wednesday":
      return 3;
    case "Thursday":
      return 4;
    case "Friday":
      return 5;
    case "Saturday":
      return 6;
    case "Sunday":
      return 7;
  }
  return -1;
}

/// returns the number of weeks in the [year]
int numOfWeeks(int year) {
  DateTime dec28 = DateTime(year, 12, 28);
  int dayOfDec28 = int.parse(DateFormat("D").format(dec28));
  return ((dayOfDec28 - dec28.weekday + 10) / 7).floor();
}

/// returns concatination of year and the week number e.g. 2021:23 => 23th week
/// of 2021
String yearWeek() {
  return Jiffy().year.toString() + ":" + Jiffy().week.toString();
}

/// returns the hour within the 168 hour week given the currentDateTime()
/// e.g. (2021:28:4:17) => 113
int dateTimeToHourInWeek(String key) {
  List<String> tokens = key.split(":");
  return int.parse(tokens[2]) * 24 + int.parse(tokens[3]);
}

/// for better (more efficient/condensed date time than DateTime.now)
String currentDateTime() {
  /// example: "2021:29:2:14"
  return Jiffy().year.toString() +
      ":" +
      Jiffy().week.toString() +
      ":" +
      weekToInt(Jiffy().EEEE).toString() +
      ":" +
      Jiffy().hour.toString();
}

int yearNumber() {
  return Jiffy().year;
}

/// Calculates week number from a date
int weekNumber() {
  return Jiffy().week;
}

/// returns a tuple string "Year:Week"  which is the next week of the
/// supplied dateTime
String nextWeekNumber(String dateTime) {
  int year = int.parse(dateTime.split(":")[0]);
  int week = int.parse(dateTime.split(":")[1]);
  // if its the first week, then previous week is 52 (or 53 if leap year)
  return week == numOfWeeks(year)
      ? (year + 1).toString() + ":1"
      : year.toString() + ":" + (week + 1).toString();
}

/// returns a tuple string "Year:Week"  which is the previous week of the
/// supplied dateTime
String previousWeekNumber(String dateTime) {
  int year = int.parse(dateTime.split(":")[0]);
  int week = int.parse(dateTime.split(":")[1]);
  /// if its the first week, then previous week is 52 (or 53 if leap year)
  return week == 1
      ? (year - 1).toString() + ":" + numOfWeeks(year - 1).toString()
      : year.toString() + ":" + (week - 1).toString();
}

/// Capitalize the first character of a String [s].
String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

/// Converts the give minutes [mins] to a neatly formatted String.
String minutesToString(int mins) {
  if (mins < 10) return "0$mins";
  return "$mins";
}