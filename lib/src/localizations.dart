import 'package:aab_localized_copy/aab_localized_copy.dart';

import 'constants.dart';

void addCopyAabGlobalFunctions() {
  addLocalizedCopy(packageCopy);
}

/// Map containing all copy for your project. This map needs to be added using
/// API call addLocalisedCopy()
Map<String, Map<String, Map<String, String>>> packageCopy = {
  packageIdentifierAabGlobalFunctions: localizedCopy
};

/// Map containing both Dutch and English copy
Map<String, Map<String, String>> localizedCopy = {
  'en': loginEnCopy,
  'nl': loginNlCopy
};

Map<String, String> loginEnCopy = {
  /*
  Month abbreviations
   */
  'january': 'Jan',
  'february': 'Feb',
  'march': 'Mar',
  'april': 'Apr',
  'may': 'May',
  'june': 'Jun',
  'july': 'Jal',
  'august': 'Aug',
  'september': 'Sep',
  'october': 'Oct',
  'november': 'Nov',
  'december': 'Dec',

  /*
  Week days
   */
  'monday': 'Monday',
  'tuesday': 'Tuesday',
  'wednesday': 'Wednesday',
  'thursday': 'Thursday',
  'friday': 'Friday',
  'saturday': 'Saturday',
  'sunday': 'Sunday',
};

Map<String, String> loginNlCopy = {
  /*
  Month abbreviations
   */
  'january': 'Jan',
  'february': 'Feb',
  'march': 'Mrt',
  'april': 'Apr',
  'may': 'Mei',
  'june': 'Jun',
  'july': 'Jul',
  'august': 'Aug',
  'september': 'Sep',
  'october': 'Okt',
  'november': 'Nov',
  'december': 'Dec',

  /*
  Week days
   */
  'monday': 'Maandag',
  'tuesday': 'Dinsdag',
  'wednesday': 'Woensdag',
  'thursday': 'Donderdag',
  'friday': 'Vrijdag',
  'saturday': 'Zaterdag',
  'sunday': 'Zondag',
};
